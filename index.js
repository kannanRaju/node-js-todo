var express = require("express");
var bodyParser = require("body-parser");
var app = express();
const con = require('./models/taskModel');
app.use(bodyParser.urlencoded({ extended: true }));
app.set("view engine", "ejs");
//render css files
app.use(express.static("public"));

//placeholders for added task
var task = ["buy socks", "practise with nodejs"];
//placeholders for removed task
var complete = ["finish jquery"];

//post route for adding new task 
app.post('/', (req, res) => {
    console.log(req.body)
    let query = "INSERT INTO Todo (task, status) VALUES ?";
    data = [
        [req.body.task, "ongoing"]
    ]
    con.query(query, [data], (err, result) => {
        if (err) throw err;
        console.log(result)
        res.redirect('/')
    })
})

app.post("/removetask", function(req, res) {
    var completeTask = req.body.check;
    //check for the "typeof" the different completed task, then add into the complete task
    if (typeof completeTask === "string") {
        complete.push(completeTask);
        //check if the completed task already exits in the task when checked, then remove it
        task.splice(task.indexOf(completeTask), 1);
    } else if (typeof completeTask === "object") {
        for (var i = 0; i < completeTask.length; i++) {
            complete.push(completeTask[i]);
            task.splice(task.indexOf(completeTask[i]), 1);
        }
    }
    res.redirect("/");
});

//render the ejs and display added task, completed task
// app.get("/", function(req, res) {
//     let query = "SELECT * FROM Todo";
//     let items = []
//     con.query(query, (err, result) => {
//         if (err) throw err;
//         items = result
//         console.log(items)
//         res.render('index', {
//             items: items
//         })
//     })
// });

app.get("/", function(req, res) {

    console.log(req.params)
    let query = "DELETE FROM Todo WHERE task_id=" + req.params.id
    console.log(query);
    con.query(query, (err, result) => {
        if (err) throw err;
        console.log(query)
        res.redirect('/')
    })
});

//set app to listen on port 3000
app.listen(3001, function() {
    console.log("server is running on port 3001");
});